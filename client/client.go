package main

import (
	"bufio"
	"bytes"
	"fmt"
	"net"
	"os"
	"sync"
	"time"
)

var wg = &sync.WaitGroup{}

type Client struct {
	server string
	port   int
}

func New(server string, port int) *Client {
	client := new(Client)
	client.server = server
	client.port = port
	return client
	//return &Client{server, port}
}

func (c Client) Start(wg *sync.WaitGroup) {
	wg.Add(1)
	go func() {
		defer wg.Done()
		// connect to this socket
		conn, _ := net.Dial("tcp", fmt.Sprintf("%s:%d", c.server, c.port))

		go handle(conn)

		fmt.Print("Start talking with the server: ")
		for {
			// read in input from stdin
			reader := bufio.NewReader(os.Stdin)
			text, _ := reader.ReadString('\n')
			// send to socket
			fmt.Fprintf(conn, text)
		}
	}()
}

func handle(conn net.Conn) {
	for {
		// listen for reply
		buf := make([]byte, 1024)
		_, err := conn.Read(buf)
		if err != nil {
			fmt.Println("ERROR: something wrong occured:", err.Error())
			return
		}
		n := bytes.Index(buf, []byte{0})
		fmt.Print("echo: " + string(buf[:n]))
	}
}

func main() {
	clientStart()
	wg.Wait()
	fmt.Println("main terminated")
}

func clientStart() {
	c := New("localhost", 8081)
	c.Start(wg)
	wg.Add(1)
	time.Sleep(100 * time.Millisecond)
}
