package main

import (
	"fmt"
	"log"
	"net"
	"sync"
	"time"
)

var wg = &sync.WaitGroup{}
var clients []net.Conn

type Server struct {
	server string
	port   int
}

func New(server string, port int) *Server {
	srv := new(Server)
	srv.server = server
	srv.port = port
	return srv
}

func (s Server) Start(wg *sync.WaitGroup) {
	wg.Add(1)
	go func() {
		defer wg.Done()
		fmt.Println("Launching server...")

		l, err := net.Listen("tcp", fmt.Sprintf("%s:%d", s.server, s.port))
		if err != nil {
			log.Fatal(err)
		}
		defer l.Close()
		log.Print("Server listening...")

		for {
			conn, err := l.Accept()
			if err != nil {
				log.Print(err)
			}

			log.Printf("Client connected %s -> %s \n", conn.RemoteAddr(), conn.LocalAddr())
			// Add the client to the connection array
			clients = append(clients, conn)

			go handle(conn)
		}
	}()
}

func handle(conn net.Conn) {
	defer removeClient(conn)
	errorChan := make(chan error)
	dataChan := make(chan []byte)

	go readWrapper(conn, dataChan, errorChan)

	for {
		select {
		case data := <-dataChan:
			log.Printf("Client %s sent: %s", conn.RemoteAddr(), string(data))
			for i := range clients {
				clients[i].Write(data)
			}
		case err := <-errorChan:
			log.Println("An error occured:", err.Error())
			return
		}
	}
}

func readWrapper(conn net.Conn, dataChan chan []byte, errorChan chan error) {
	for {
		buf := make([]byte, 1024)
		reqLen, err := conn.Read(buf)
		if err != nil {
			errorChan <- err
			return
		}
		dataChan <- buf[:reqLen]
	}
}

func removeClient(conn net.Conn) {
	log.Printf("Client %s disconnected", conn.RemoteAddr())
	conn.Close()
	//remove client from clients here
	var copyClients []net.Conn
	for i := range clients {
		var c = clients[i]
		if clients[i] == conn {
			continue
		} else {
			copyClients = append(copyClients, c)
		}
	}
	clients = copyClients
}

func main() {
	serverStart()
	wg.Wait()
	fmt.Println("main terminated")
}

func serverStart() {
	s := New("localhost", 8081)
	s.Start(wg)
	wg.Add(1)
	time.Sleep(100 * time.Millisecond)
}
